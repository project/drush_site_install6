<?php

/**
 * Implementation of hook_drush_command().
 */
function drush_site_install6_drush_command() {
  $items['site-install6'] = array(
    'description' => 'Install Drupal 6.x site using the default install profile. Please note that this module assumes that settings.php file exists and database specifications have been already defined.',
    'options' => array(
      'account-name' => 'uid1 name. defaults to admin',
      'account-pass' => 'uid1 pass. defaults to admin',
      'account-mail' => 'uid1 email. defaults to admin@example.com',
      'locale' => 'A short language code. Sets the default site language. Language files must already be present. You may use download command to get them.',
      'profile' => 'Install profile to use. Defaults to default',
      'clean-url'=> 'Defaults to 1',
      'site-name' => 'Defaults to Site-Install',
      'site-mail' => 'From: for system mailings. Defaults to admin@example.com',
    ),
    'examples' => array(
      'drush site-install6 --locale=uk' => '(Re)install using the default install profile. Set default language to Ukranian.',
      'drush site-install6 --account-name=joe --account-pass=mom' => 'Re-install with specified uid1 credentials.',
    ),
    'core' => array(6),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_ROOT,
    'aliases' => array('si6')
  );

  return $items;
}

/**
 * Implementation of hook_drush_help().
 */
function drush_site_install6_drush_help($section) {
  if ($section == 'drush:site-install6') {
    return dt("Install Drupal 6.x.");
  }
}

/**
 * Drush callback; install Drupal 6.x
 */
function drush_drush_site_install6_pre_site_install6() {
  drush_core_pre_site_install();
}

/**
 * Drush callback; install Drupal 6.x
 */
function drush_drush_site_install6_site_install6() {  
  $drupal_root = drush_get_context('DRUSH_DRUPAL_ROOT');
  
  $phpcode = _drush_site_install6_cookies(). ' include("'. $drupal_root .'/install.php");';
  drush_shell_exec('php -r %s', $phpcode);
  $cli_output = drush_shell_exec_output();
  $cli_cookie = end($cli_output);
  
  $phpcode = _drush_site_install6_cookies($cli_cookie). ' $_GET["op"]="start"; include("'. $drupal_root .'/install.php");';
  drush_shell_exec('php -r %s', $phpcode);

  $phpcode = _drush_site_install6_cookies($cli_cookie). ' $_GET["op"]="do_nojs"; include("'. $drupal_root .'/install.php");';
  drush_shell_exec('php -r %s', $phpcode);

  $phpcode = _drush_site_install6_cookies($cli_cookie). ' $_GET["op"]="finished"; include("'. $drupal_root .'/install.php");';
  drush_shell_exec('php -r %s', $phpcode);

  $account_pass = drush_get_option('account-pass', 'admin');
  $phpcode = _drush_site_install6_cookies($cli_cookie);
  $phpcode .= '
  $_POST = array (
    "site_name" => "'. drush_get_option('site-name', 'Site-Install') .'", 
    "site_mail" => "'. drush_get_option('site-mail', 'admin@example.com') .'", 
    "account" => array (
      "name" => "'. drush_get_option('account-name', 'admin') .'",
      "mail" => "'. drush_get_option('account-mail', 'admin@example.com') .'",
      "pass" => array (
        "pass1" => "'. $account_pass .'",
        "pass2" => "'. $account_pass .'"
      )
    ),
    "date_default_timezone"=>"0", 
    "clean_url"=>'. drush_get_option('clean-url', TRUE) .',
    "form_id"=>"install_configure_form", 
    "update_status_module" => array("1"=>"1")
  );
  include("'. $drupal_root .'/install.php");';
  drush_shell_exec('php -r %s', $phpcode);
}

/**
 * Utility function to grab/set current "cli cookie".
 * 
 */
function _drush_site_install6_cookies($cookie = NULL) {
  $output = '$_GET=array("profile"=>"'. drush_get_option('profile', 'default') .'", "locale"=>"' . drush_get_option('locale', 'en') . '", "id"=>"1"); $_REQUEST=&$_GET;';
  $output .= 'define("DRUSH_SITE_INSTALL6", TRUE);';
  
  if ($cookie) {
    $output .= sprintf('$_COOKIE=unserialize("%s");', str_replace('"', '\"', $cookie));
  }
  else {
    $output .= 'function _cli_cookie_print(){print(serialize(array(session_name()=>session_id())));}
register_shutdown_function("_cli_cookie_print");';
  }
  
  return $output;
}
